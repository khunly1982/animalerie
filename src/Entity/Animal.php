<?php

namespace App\Entity;

use App\Repository\AnimalRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AnimalRepository::class)
 */
class Animal
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=15)
     * @Assert\NotBlank()
     * @Assert\Length(max=15, min=15)
     */
    private $chipNumber;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     * @Assert\Positive()
     * @Assert\Range(min=2000, max=3000)
     */
    private $birthYear;

    /**
     * @ORM\Column(type="blob", nullable=true)
     */
    private $picture;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $mimeType;

    /**
     * @ORM\Column(type="boolean")
     */
    private $readyForAdoption;

    /**
     * @var UploadedFile
     * @Assert\File(mimeTypes={"image/jpeg","image/jpg", "image/png"}, maxSize="20000000")
     */
    private $file;

    /**
     * @ORM\ManyToOne(targetEntity=Breed::class)
     * @ORM\JoinColumn(name="breed_id", nullable=true, onDelete="SET NULL")
     */
    private $breed;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChipNumber(): ?string
    {
        return $this->chipNumber;
    }

    public function setChipNumber(string $chipNumber): self
    {
        $this->chipNumber = $chipNumber;

        return $this;
    }

    public function getBirthYear(): ?int
    {
        return $this->birthYear;
    }

    public function setBirthYear(int $birthYear): self
    {
        $this->birthYear = $birthYear;

        return $this;
    }

    public function getPicture()
    {
        return $this->picture;
    }

    public function setPicture($picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * @param mixed $mimeType
     * @return Animal
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;
        return $this;
    }



    public function getBreed(): ?Breed
    {
        return $this->breed;
    }

    public function setBreed(?Breed $breed): self
    {
        $this->breed = $breed;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReadyForAdoption()
    {
        return $this->readyForAdoption;
    }

    /**
     * @param mixed $readyForAdoption
     * @return Animal
     */
    public function setReadyForAdoption($readyForAdoption)
    {
        $this->readyForAdoption = $readyForAdoption;
        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param UploadedFile $file
     * @return Animal
     */
    public function setFile(UploadedFile $file): Animal
    {
        $this->file = $file;
        return $this;
    }

    public function getPicture64() {
        // convertir un blob en base 64
        if($this->getPicture() != null) {
            return base64_encode(stream_get_contents($this->getPicture()));
        }
        return null;
    }


}
