<?php

namespace App\Controller;

use App\Entity\Animal;
use App\Form\AnimalType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AnimalController extends AbstractController
{
    /**
     * @Route("/", name="animal_index")
     */
    public function index(): Response
    {
        $repo = $this->getDoctrine()->getRepository(Animal::class);
        $animals = $repo->findAllWithBreed();
        return $this->render('animal/index.html.twig', [
            'animals' => $animals
        ]);
    }

    /**
     * @Route("/admin/animal/add", name="animal_add")
     */
    public function add(Request $req) {
        $animal = new Animal();
        $form = $this->createForm(AnimalType::class, $animal);
        $form->handleRequest($req);
        if($form->isSubmitted() && $form->isValid()) {
            // récupérer le fichier et le transformer en blob
            if($animal->getFile() != null) {
                $bin = file_get_contents($animal->getFile()->getPathname());
                $animal->setPicture($bin);
                $animal->setMimeType($animal->getFile()->getMimeType());
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($animal);
            $em->flush();
            $this->addFlash('success', 'bravo');
            return $this->redirectToRoute('animal_index');
        }
        return $this->render('animal/add.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
