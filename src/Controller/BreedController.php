<?php

namespace App\Controller;

use App\Entity\Breed;
use App\Form\BreedType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class BreedController extends AbstractController
{
    /**
     * @Route("/breed", name="breed_index")
     */
    public function index(): Response
    {
        $repo = $this->getDoctrine()->getRepository(Breed::class);

        $breeds = $repo->findAll();

        return $this->render('breed/index.html.twig', [
            'breeds' => $breeds,
        ]);
    }

    /**
     * @Route("/breed/add", name="breed_add")
     */
    public function add(Request $req): Response
    {
        // création d'un breed vide
        $breed = new Breed();
        // créer le formulaire
        $form = $this->createForm(BreedType::class, $breed);
        // permet au fomulaire de gérer la requête
        $form->handleRequest($req);
        if($form->isSubmitted() && $form->isValid())
        // post // validation
        {
            // enregistrer dans la db
            $em = $this->getDoctrine()->getManager();
            $em->persist($breed);
            $em->flush();

            // le flash bag permet d'enregistrer des messages temporaires
            $this->addFlash('success', 'L\'enregistrement a bien été effectué');

            // redirection
            return $this->redirectToRoute('breed_index');
        }
        if($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('error', 'Le formulaire n\'est pas valide');
        }
        // afficher le formulaire
        return $this->render('breed/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/breed/delete/{breed}", name="breed_delete", requirements={ "breed": "\d+" })
     */
    public function delete(Breed $breed) { // param converter SELECT * FROM Breed WHERE id = :breed
        if($breed == null){
            throw new NotFoundHttpException();
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($breed);
        $em->flush();

        $this->addFlash('success', 'L\'élément a été correctement supprimer');

        return $this->redirectToRoute('breed_index');
    }

    /**
     * @Route("/breed/edit/{breed}", name="breed_edit")
     */
    public function edit(Request $req, Breed $breed) {
        if($breed == null){
            throw new NotFoundHttpException();
        }
        $form = $this->createForm(BreedType::class, $breed);
        // permet au fomulaire de gérer la requête
        $form->handleRequest($req);
        if($form->isSubmitted() && $form->isValid())
            // post // validation
        {
            // enregistrer dans la db
            $em = $this->getDoctrine()->getManager();
            $em->persist($breed);
            $em->flush();

            // le flash bag permet d'enregistrer des messages temporaires
            $this->addFlash('success', 'L\'enregistrement a bien été effectué');

            // redirection
            return $this->redirectToRoute('breed_index');
        }
        if($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('error', 'Le formulaire n\'est pas valide');
        }
        // afficher le formulaire
        return $this->render('breed/add.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
