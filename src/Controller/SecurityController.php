<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\RawMessage;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $req,
                             UserPasswordEncoderInterface $encoder,
                             MailerInterface $mailer
    ) {
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($req);
        if($form->isSubmitted() && $form->isValid()) {
            /* specific pour les user */
            $salt = uniqid();
            $user->setSalt($salt);
            $encoded = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($encoded);
            $user->setRole('ROLE_CUSTOMER'); // doivent commencer par ROLE_
            /* specific pour les user */
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            /** envoi d'email */
            $email = new Email();
            $email->addTo($user->getEmail());
            $email->subject('Bienvenue sur notre animalerie');
            $email->Html('<p>test</p>');
            $email->from('doej23398@gmail.com');
            $mailer->send($email);
            /** end envoi  */
            $this->addFlash('success', 'enregistrement OK');
            return $this->redirectToRoute('animal_index');
        }
        return $this->render('security/register.html.twig', [
            'form' => $form->createView()
        ]);

        // hashage pas de inversion possible vs cryptage inversion possible
        // password toujours hasher !!!
    }
}
